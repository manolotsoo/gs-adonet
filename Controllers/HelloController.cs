using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class HelloController : Controller
    {
        private readonly ILogger<HelloController> _logger;

        public HelloController(ILogger<HelloController> logger)
        {
            _logger = logger;
            // String hw = "Hello world Man";
            logger.Log(LogLevel.Information, "{hw}!", "Hello world inline");
            // logger.LogInformation("Hello World! Logging is {Description}.", "fun");
        }

        public IActionResult Index(String name, int count)
        {
            ViewData["name"] = name;
            ViewData["count"] = count;
            Role role = new Role()
            {
                name = "Administrator"
            };
            return View(role);
        }

        public String detail(string? name, int? age)
        {
            return $"Detail Result {name} age {age}";
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View("Error!");
        }
    }
}